<?php

/**
 * @file
 * Image Style Flush Cron module.
 *
 * Flush image styles you choose when cron runs.
 */

/**
 * Valid permissions for this module.
 *
 * @return array
 *   An array of valid permissions for the imagestyleflush_cron module
 */
function imagestyleflush_cron_perm() {
  return array('administer imagestyleflush_cron settings');
}

/**
 * Define a systema admin page.
 *
 * @return array
 *   An array of module settings parameters
 */
function imagestyleflush_cron_admin() {
  // Init options array.
  $options = array();

  // Get all image styles.
  $styles = image_styles();
  /*
  echo '<pre>';
  var_export($styles);
  echo '</pre>';
   */
  foreach ($styles as $style) {
    $options[] = $style['name'];
  }

  // Define a set of chechboxes, one for each image style configured.
  $form['imagestyleflush_cron_styles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Styles to flush'),
    '#default_value' => variable_get('imagestyleflush_cron_styles', array()),
    '#options' => $options,
    '#description' => t("Choose the image styles to flush when cron runs. Styles set by modules may override these settings."),
  );

  return system_settings_form($form);
}

/**
 * TODO imagestyleflush_cron_menu description.
 *
 * @return array
 *   An array of menu settings
 */
function imagestyleflush_cron_menu() {
  $items['admin/config/media/image-styles/flush-cron'] = array(
    'title' => 'Flush style(s) on cron run',
    'description' => 'Choose the image styles to flush when cron runs. Styles set by modules may override these settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('imagestyleflush_cron_admin'),
    'access arguments' => array('administer image styles'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Implements hook_cron().
 */
function imagestyleflush_cron_cron() {
  // Lets clean the builded image on cron run to prevent full harddisk on
  // super large wallpaper site.
  //
  // Lets get the image style name for this site.
  $styles = image_styles();
  // List of styles you want to flush.
  $remove = variable_get('imagestyleflush_cron_styles', array());
  // Lets us loop through the style array and determine if that style should
  // be flushed or not.
  foreach ($styles as $style) {
    if (in_array($style['name'], $remove)) {
      image_style_flush($style);
    }
  }
}
