
Image style flush cron
==================

This module will allow Drupal to flush image styles on cron run,
selected from the administrative interface.


Features
--------

  - Select image styles to flush on cron run


Dependencies
------------

The image (core) module.


Install
-------

1) Copy the imagestyleflush_cron folder to the
   modules folder in your installation.
   Usually this is sites/all/modules.
   Or use the UI and install it via admin/modules/install.

2) In your Drupal site, enable the module under Administration -> Modules
   (/admin/modules).


Usage
-----

You can flush image styles under Administration -> Configuration -> Media
-> Image styles -> Flush Cron


Known problems
--------------

Private file image styles can't be flushed with this module.


Credit
------

This module was written by Mike Bybee,
inspired by imagestyleflush
(by Stepan Kuzmin and maintained by Hargobind Khalsa)
and imagecache_cron_flush by Roberto Peruzzo.
